//syntax of jQuery 
//$(selector).member

//the below function is a document ready function
//it is invoked when all the dom is loaded
//$(document).ready(function(){
//  alert("hello");
//});

//but jquery also have a short hand version for document ready event, which is degined as follows

/*
$(function(){
    //body of doc ready
});
*/

$(function(){
//    //alert("hello");
//    //$("#box").hide();
//    $(".thing").fadeOut(1000);
//    $("button").click(function(){
//        $("#box").fadeOut(1000);
//    });
//selector
//grouping selector
///$("h2, h3, h4").css("border", "solid 1px red");
//
////id selector
//$("div#container").css("border", "solid 1px green");
//
////class selector
//$(".lead").css("border", "dashed 1px red");
//
////descendant selector
//$("div em").css("border", "solid 1px red");
//
////child selector
//$("div>p").css("border", "solid 1px blue");
//
////pseudo-selector
//$("li:first").css("border", "solid 1px pink");
//
////all even elements
//$("p:even").css("border", "solid 1px pink");
//
////all heading
//$(":header").css("text-align", "center");
//
////jQuery contains selector
//$("div:contains('love')").css("border", "solid 1px green");*/

//events
//$("#box").click(function(){
//    alert("you just clicked me");
//});
//
//$("input[type='text']").blur(function(){
//    if($(this).val() === ""){
//        $(this).css("border", "solid 1px red");
//        $("#box").text("forgot to add something");
//    }
//});
//
//$("input[type='text']").keydown(function(){
//    if($(this).val() !== ""){
//        $(this).css("border", "solid 1px green");
//        $("#box").text("thanks for this buddy");
//    }
//});
//
//$("#box").hover(function(){
//    $(this).text("you hovered in");
//}, function(){
//    $(this).text("you hovered out");
//});...............accpets two functions

//$("#box).hover(null,function(){
//..............this will be only for hover out
//});

//chaining
//
$(function(){
    $(".notification-bar").delay(2000).slideDown().delay(3000).slideUp();
});
/******************css in jquery********************/

//$("#circle2").css({
//    'display': 'inline-block',
//    'background': '#8a8',
//    'color': 'white',
//    'text-align': 'center',
//    'line-height': '140px',
//    'width': '140px',
//    'height': '140px',
//    'margin': '40px',
//}).addClass('circleShape');
//
//$("#name").blur(function(){
//    if($(this).val() == ""){
//        $(this).addClass("danger");
//    }else{
//        $(this).removeClass("danger");
//    }
//});
//
//    // $('h1').hide();
//    //$(".hidden").show();
//    //$('div.hidden').fadeIn(1000);
//    
//    $("#box1").click(function(){
//        $(this).fadeTo(3000, 0.25, function(){
//            $(this).slideUp();
//        })
//    });
//    
//    $(".hidden").slideDown();
//    
//    $('button').click(function(){
//        $("#box1").slideToggle();
//    });
    
    $("#left").click(function(){
        $(".box").animate({
            left: "-=40px",
            fontSize: "+=2px"
        });
    });
    
     $("#right").click(function(){
        $(".box").animate({
            left: "+=40px",
            fontSize: "-=2px"
        });
    });
    
     $("#up").click(function(){
        $(".box").animate({
            top: "-=40px",
            opacity: "+=0.1"
        });
    });
    
     $("#down").click(function(){
        $(".box").animate({
            top: "+=40px",
            opacity: "-=0.1"
        });
    });
    
    //******************car race*************************
    
    $("#go").click(function(){
        
        function checkIfComplete(){
            if(isComplete == false){
                isComplete = true;
            }else{
                place = "second";
            }
        }
        
        let isComplete = false;
        
        let place = 'first';
        
        //bring racetrack width
        let raceTrackWidth = $(document).width()-$("#car1").width();
        
        //2 random time for animating car
        let raceTime1 = Math.floor(1 + Math.random() * 5000);
        let raceTime2 = Math.floor(1 + Math.random() * 5000);
        
        $("#car1").animate({
            left: raceTrackWidth
            }, raceTime1, function(){
            checkIfComplete();
            $("#raceInfo1 span").text("finished in " + place + " place and clocked in at " + raceTime1 + " ms");
        });
        
        $("#car2").animate({
            left: raceTrackWidth
            }, raceTime2, function(){
            checkIfComplete();
            $("#raceInfo2 span").text("finished in " + place + " place and clocked in at " + raceTime2 + " ms");
        });
    });//go click over
    
    $("#reset").click(function(){
        $(".car").css("left", "0");
        $(".raceInfo span").text("");
    })
});





































